'use strict';

describe('Service: Schedule', function () {

  // load the service's module
  beforeEach(module('offpremiumHomeEditorApp'));

  // instantiate service
  var Schedule;
  beforeEach(inject(function (_Schedule_) {
    Schedule = _Schedule_;
  }));

  it('should do something', function () {
    expect(!!Schedule).toBe(true);
  });

});
