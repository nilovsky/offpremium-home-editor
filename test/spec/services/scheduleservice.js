'use strict';

describe('Service: Scheduleservice', function () {

  // load the service's module
  beforeEach(module('offpremiumHomeEditorApp'));

  // instantiate service
  var Scheduleservice;
  beforeEach(inject(function (_Scheduleservice_) {
    Scheduleservice = _Scheduleservice_;
  }));

  it('should do something', function () {
    expect(!!Scheduleservice).toBe(true);
  });

});
