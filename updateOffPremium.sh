#!/bin/bash

if [[ -z "$OFFPREMIUM_HOME" ]]; then
    echo 'Variável $OFFPREMIUM_HOME não definida'
    echo 'Configure seu .bashrc de modo que ela aponte para a raiz do projeto da loja OffPremium'
    exit
fi

echo 'Copying images'
cp dist/images/*.* $OFFPREMIUM_HOME/web/images

echo 'Copying styles'
cp dist/styles/*.* $OFFPREMIUM_HOME/web/styles

echo 'Copying fonts'
cp bower_components/bootstrap/fonts/*.* $OFFPREMIUM_HOME/web/fonts

echo 'Copying scripts'
cp dist/scripts/*.* $OFFPREMIUM_HOME/web/scripts

echo 'Copying views'
cp dist/views/*.* $OFFPREMIUM_HOME/web/views

echo 'Copying index.html'
cp dist/index.html $OFFPREMIUM_HOME/web/WEB-INF/jsp/homeEditor
