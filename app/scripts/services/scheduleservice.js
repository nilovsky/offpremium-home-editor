'use strict';

/**
 * @ngdoc service
 * @name offpremiumHomeEditorApp.Scheduleservice
 * @description
 * # Scheduleservice
 * Service in the offpremiumHomeEditorApp.
 */
angular.module('offpremiumHomeEditorApp')
  .service('scheduleService', function scheduleService($log, $http, $q) {
    return {
    	listScheduledTemplates: function() {
    		var deferred = $q.defer();

    		$http.get('HomeSchedule.action')
    		.success(function(data) {
    			deferred.resolve(data);
    		}).error(function(data) {
    			deferred.reject(data);
    		});

    		return deferred.promise;
    	},

        removeScheduledTemplate: function(template) {
            var deferred = $q.defer();

            $http({
                method: 'post',
                url: 'RemoveScheduledTemplate.action',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({'template': template})
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data) {
                $log.error('Could not remove schedule entry. Reason: '+data);
            });

            return deferred.promise;
        }
    };
  });
