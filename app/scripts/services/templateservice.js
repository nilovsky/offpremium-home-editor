'use strict';

angular.module('offpremiumHomeEditorApp')
  .service('templateService', function templateService($log, $http, $q, $filter) {
    return {
    	findSpotByPosition: function(position) {
    		var spot = this.getActiveTemplate().spots[position];

    		if (typeof spot !== 'object') {
    			spot = {
    				id: 0,
    				position: position
    			};

    			this.data.spots[position] = spot;
			}

			return spot;
    	},
    	getActiveTemplate: function() {
    		var deferred = $q.defer();

    		$http.get('LoadHomeTemplate.action')
    		.success(function(data) {
    			deferred.resolve(data);
    		}).error(function(data) {
    			deferred.reject(data);
    		});

    		return deferred.promise;
    	},
    	save: function (template) {
    		var deferred = $q.defer();

    		$http({
    			method: 'post',
    			url: 'SaveSpot.action',
    			headers: {'Content-Type': 'application/json'},
    			data: JSON.stringify({'template': template})
    		}).success(function(data) {
    			deferred.resolve(data);
    		}).error(function(data) {
    			deferred.reject(data);
    		});

    		return deferred.promise;
    	},
    	publish: function(template) {
    		var deferred = $q.defer();

            template.dateAvailable.setHours(
                template.dateAvailable.getHours() - template.dateAvailable.getTimezoneOffset() / 60
            );
            
            $http({
    			method: 'post',
    			url: 'PublishTemplate.action',
    			headers: {'Content-Type': 'application/json'},
    			data: JSON.stringify({'template': template})
    		}).success(function() {
                template.dateAvailable.setHours(
                    template.dateAvailable.getHours() + template.dateAvailable.getTimezoneOffset() / 60
                );
    			deferred.resolve({ type: 'success', msg: 'A vitrine agendada para '+$filter('date')(template.dateAvailable, 'dd/MM/yyyy H:mm') });
    		}).error(function() {
    			deferred.resolve({ type: 'danger', msg: 'A vitrine não pôde ser agendada' });
    		});

    		return deferred.promise;
    	}
    };
  });
