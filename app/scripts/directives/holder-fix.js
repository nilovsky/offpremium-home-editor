'use strict';

/**
 * @ngdoc directive
 * @name offpremiumHomeEditorApp.directive:holderFix
 * @description
 * # holderFix
 */
angular.module('offpremiumHomeEditorApp')
  .directive('holderFix', function () {
    return {
        link: function (scope, element) {
            Holder.run({ images: element[0], nocss: true });
        }
    };
  });
