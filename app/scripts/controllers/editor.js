'use strict';

angular.module('offpremiumHomeEditorApp')
  .controller('EditorCtrl', function ($scope, $modal, $log, templateService, activeTemplate) {

    $scope.editSpot = function editSpot(position) {
      var view = 'spotEditorContent.html';

      $scope.selectedSpot = activeTemplate.spots[position];

      if ($scope.selectedSpot.type === 'SLIDER') {
        view = 'sliderEditorContent.html'; 
      }

      $modal.open({
        templateUrl: view,
        controller: 'ModalInstanceCtrl',
        resolve: {
          spot: function() {
            return $scope.selectedSpot;
          },
          activeTemplate: function() {
            return $scope.activeTemplate;
          }
        }
      });
    };

    $scope.getSpot = function getSpot(position) {
      return $scope.activeTemplate.spots[position];
    };

    $scope.selectedSpot = {};
    $scope.activeTemplate = activeTemplate;
  });