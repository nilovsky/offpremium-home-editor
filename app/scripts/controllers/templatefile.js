'use strict';

angular.module('offpremiumHomeEditorApp')
.controller('TemplateFileCtrl', function ($scope, $window, $modal, $log, $location, templateService) {

    $scope.preview = function preview() {
    	$window.open('PreviewTemplate.action');
    };

    $scope.openPublish = function openPublish() {
    	$modal.open({
    		templateUrl: 'publishContent.html',
    		controller: 'ModalInstanceCtrl',
    		resolve: {
    			spot: function() {
    				return {};
    			},
                activeTemplate: function() {
                    return templateService.getActiveTemplate();
                }
    		}
    	});
    };

    $scope.openSchedule = function openSchedule() {
        $location.path('/schedule');
    };
});