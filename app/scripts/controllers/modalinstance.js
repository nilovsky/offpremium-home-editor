'use strict';

angular.module('offpremiumHomeEditorApp')
.controller('ModalInstanceCtrl', function($scope, $modalInstance, $log, $upload, $filter, spot, activeTemplate, templateService) {

    $scope.publishTime = '';
    $scope.spot = spot;
    $scope.template = activeTemplate;
    $scope.sliderUrls = spot.url ? spot.url.split(';') : [];
    $scope.sliderImgs = spot.imagePath ? spot.imagePath.split(';') : [];
    $scope.dateAvailable = new Date();
    $scope.dateFormat = 'dd/MM/yyyy';
    $scope.brands = [
      {id: 'a-brand', label: 'A-BRAND'},
      {id: 'animale', label: 'ANIMALE'},
      {id: 'farm', label: 'FARM'},
      {id: 'fyi', label: 'FYI'},
      {id: 'fabula', label: 'FÁBULA'}
    ];
    $scope.alerts = [];
    $scope._uploadFinished = false;
    $scope._uploadProgress = false;

    $scope.saveSpot = function saveSpot() {
      if ($scope.spot.type === 'SLIDER') {
        $scope.spot.url = $scope.sliderUrls.join(';');
        $scope.spot.imagePath = $scope.sliderImgs.join(';');
      }

      templateService.save($scope.template);
      $modalInstance.close($scope.spot);
    };

    $scope.cancel = function cancel() {
    	$modalInstance.dismiss('cancel');
    };

    $scope.publish = function publish(time) {
      var splittedTime = time.split(':');
      $scope.template.dateAvailable.setHours(splittedTime[0]);
      $scope.template.dateAvailable.setMinutes(splittedTime[1]);

      templateService.publish($scope.template)
      .then(function(alert) {
        $log.log(alert);
        $scope.alerts.push(alert);
      });
    };

    $scope._uploadProgressCallback = function() {
      $scope._uploadProgress = true;
    };

    $scope._uploadFinishedCallback = function() {
      $scope._uploadProgress = false;
      $scope._uploadFinished = true;
    };

    $scope.onFileSelect = function($files, updateImage) {
      $log.log($files);
      for (var i = 0; i < $files.length; i++) {
        var file = $files[i];
        $log.log(file);
        $scope._addImageToSpot(file.name, updateImage, i);
        $scope.upload = $upload.upload({
          url: 'UploadHomeImage.action',
          file: file,
        })
        .progress($scope._uploadProgressCallback)
        .success($scope._uploadFinishedCallback);
      }
    };

    $scope._addImageToSpot = function _addImageToSpot(filename, updateImage, imgIndex) {
      if ($scope.spot.type === 'SLIDER') {
        if (updateImage) {
          $scope.sliderImgs[imgIndex] = filename;
        } else {
          $scope.sliderImgs.push(filename);
        }
        return;
      }

      $scope.spot.imagePath = filename;
    };

    $scope.removeSliderPosition = function removeSliderImage(index) {
      $scope.sliderImgs.splice(index, 1);
      $scope.sliderUrls.splice(index, 1);
    };

    $scope.closeAlert = function closeAlert() {
    	$scope.alerts = [];
    };
});