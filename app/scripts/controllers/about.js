'use strict';

/**
 * @ngdoc function
 * @name offpremiumHomeEditorApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the offpremiumHomeEditorApp
 */
angular.module('offpremiumHomeEditorApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
