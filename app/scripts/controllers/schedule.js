'use strict';

/**
 * @ngdoc function
 * @name offpremiumHomeEditorApp.controller:ScheduleCtrl
 * @description
 * # ScheduleCtrl
 * Controller of the offpremiumHomeEditorApp
 */
angular.module('offpremiumHomeEditorApp')
  .controller('ScheduleCtrl', function ($scope, $log, $modal, $window, scheduledTemplates, scheduleService) {

  	$scope.scheduledTemplates = scheduledTemplates;

  	$scope.remove = function remove(template) {
  		scheduleService.removeScheduledTemplate(template).then(function(templates) {
  			$scope.scheduledTemplates = templates;
  		});
  	};

  	$scope.openPublishDateModal = function openPublishDateModal(template) {
  		$modal.open({
        	templateUrl: 'publishContent.html',
        	controller: 'ModalInstanceCtrl',
        	resolve: {
        		spot: function() {
        			return {};
        		},
          		activeTemplate: function() {
            		return template;
          		}
        	}
      	});
  	};

  	$scope.preview = function preview(template) {
  		$window.open('PreviewTemplate.action?id='+template.id);
  	};
  });
