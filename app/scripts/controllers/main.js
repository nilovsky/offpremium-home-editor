'use strict';

/**
 * @ngdoc function
 * @name offpremiumHomeEditorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the offpremiumHomeEditorApp
 */
angular.module('offpremiumHomeEditorApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
