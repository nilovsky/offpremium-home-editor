'use strict';

/**
 * @ngdoc overview
 * @name offpremiumHomeEditorApp
 * @description
 * # offpremiumHomeEditorApp
 *
 * Main module of the application.
 */
angular
  .module('offpremiumHomeEditorApp', [
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'angularFileUpload'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/editor', {
        templateUrl: 'views/editor.html',
        controller: 'EditorCtrl',
        resolve: {
          activeTemplate: function(templateService) {
            return templateService.getActiveTemplate();
          }
        }
      })
      .when('/schedule', {
        templateUrl: 'views/schedule.html',
        controller: 'ScheduleCtrl',
        resolve: {
          scheduledTemplates: function(scheduleService) {
            return scheduleService.listScheduledTemplates();
          }
        }
      })
      .otherwise({
        redirectTo: '/'
      });
  });